var React = require('react');
var ReactNative = require('react-native');
var {AppRegistry, StyleSheet, Text, View, Component, TouchableOpacity, BackAndroid} = ReactNative;
var router = require('react-native-router-flux');
var {Scene, Router, Switch, TabBar, Modal, Schema, Actions} = router;
var Icon = require('react-native-vector-icons/FontAwesome');
const SideMenu = require('react-native-side-menu');
var Menu = require('./components/Menu')
var loginPage = require('./components/loginPage');
var main = require('./components/main');
var surveys = require('./components/Surveys');
var plans = require('./components/Plans');
var responses = require('./components/Responses');
var forms = require('./components/Forms');
var portfolios = require('./components/portfolios');
var portfolioPlan = require('./components/PortfolioPlan');
var planList = require('./components/PlanList');
var styles = StyleSheet.create({
    header: {
        backgroundColor: '#38ACEC'
    },
    menuButton: {
        position: 'absolute',
        top: 20,
        padding: 10
    },
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF'
    }
});

class MenuButton extends React.Component {
  handlePress(e) {
    if (this.props.onPress) {
      this.props.onPress(e);
    }
  }

  render() {
    return (
      <TouchableOpacity
        onPress={this.handlePress.bind(this)}
        style={this.props.style}>
        <Text>{this.props.children}</Text>
      </TouchableOpacity>
    );
  }
}

const scenes = Actions.create(
    <Scene key="modal" component={Modal}>
        <Scene key="root" hideNavBar={true}>
            <Scene key="loginPage" component={loginPage} />
            <Scene key="portfolios" component={portfolios} />
            <Scene key="portfolioPlan" component={portfolioPlan} />
            <Scene key="main" component={main} />
            <Scene key="surveys" component={surveys} />
            <Scene key="responses" component={responses} />
            <Scene key="planList" component={planList} />
            <Scene key="plans" component={plans} />
            <Scene key="forms" component={forms} />

        </Scene>
    </Scene>
);

module.exports = class mdcaMobile extends React.Component{
    state = {
      isOpen: false,
      selectedItem: 'About',
    }

    toggle() {
      this.setState({
        isOpen: !this.state.isOpen,
      });
    }

    updateMenuState(isOpen) {
      this.setState({ isOpen, });
    }

    componentWillMount = () => {
      BackAndroid.addEventListener('hardwareBackPress', () => Actions.pop());
    };

    onMenuItemSelected = (item) => {
      this.setState({
        isOpen: false,
        selectedItem: item,
      });
    };
    render() {
        const menu = <Menu onItemSelected={this.onMenuItemSelected} />;

        return (
            <SideMenu
              menu={menu}
              negotiatePan={true}
              isOpen={this.state.isOpen}
              onChange={(isOpen) => this.updateMenuState(isOpen)}>
                  <Router scenes={scenes}/>
              <MenuButton style={styles.menuButton} onPress={() => this.toggle()}>
                <Icon name="bars" size={24} color="#38ACEC" />
              </MenuButton>
            </SideMenu>
        );
    }
}
