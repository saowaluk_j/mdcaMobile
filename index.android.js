import React, {
  Component,
} from 'react';
import {
  AppRegistry,
  Image,
  ListView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
var mdcaMobile = require('./mdcaMobile');

AppRegistry.registerComponent('mdcaMobile', () => mdcaMobile);
