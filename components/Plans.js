var React = require('react');
var ReactNative = require('react-native');
var Button = require('react-native-button');
var Icon = require('react-native-vector-icons/FontAwesome');
var Actions = require('react-native-router-flux').Actions;
var Accordion = require('react-native-collapsible/Accordion');
var MapView = require('react-native-maps');
const {
  StyleSheet,
  Text,
  View,
  Image,
  ListView,
  TouchableOpacity,
  Component,
  TouchableHighlight,
  ScrollView,
  Dimensions,
  Linking,
  Alert,
} = ReactNative;

var { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const styles = StyleSheet.create({
    menuButton: {
        position: 'absolute',
        top: 20,
        padding: 10
    },
    caption: {
        fontSize: 20,
        fontWeight: 'bold',
        alignItems: 'center'
    },
    buttonContainer: {
         padding:10,
         height:45,
         borderRadius:4,
         backgroundColor: '#38ACEC',
         margin: 20
    },
    button: {
         fontFamily: 'Cochin',
         color: '#FFF'
    },
    head: {
         marginLeft: 50,
         marginRight: 30,
         marginTop: 20,
         alignItems: 'flex-end'
    },
    headContainerRight: {
         flexDirection: 'row',
         padding: 3
    },
    icon: {
        marginRight:5
    },
    headContainerLeft: {
         alignSelf: 'stretch',
         marginTop: 5,
         marginLeft: 10
    },
    item: {
         fontFamily: 'Cochin',
         fontSize: 22,
         fontWeight: 'bold',
         padding: 2,
         color: '#222222'
    },
    containerMap: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
  },
  eventList: {
    position: 'absolute',
    top: height / 2,
    left: 0,
    right: 0,
    bottom: 0,
  },
  container: {
       alignItems: 'stretch',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: height / 2,
  },
});

var Plans = React.createClass ({
    getInitialState() {
        return {

        }
    },

    openGoogleMap (latLng) {
        Linking.canOpenURL('google.navigation:q='+latLng.lat+','+latLng.lng).then(supported => {
          if (supported) {
            Linking.openURL('google.navigation:q='+latLng.lat+','+latLng.lng);
          } else {
            console.log('Cannot open URI: ' + this.props.url);
          }
        });
    },


   _renderContent(rowData) {
     return (
           <View style={{
              backgroundColor: '#368BC1',
              flexDirection: 'row',
              padding: 3,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
              <TouchableHighlight style={{
                   marginRight:30,
                   margin:10
                    }}
                   onPress={(event)=> {
                   Alert.alert(
                       'Direction',
                       'Do you want to get directions on Google map Application?',
                       [
                         {text: 'Ok', onPress: () => this.openGoogleMap(rowData.location)},
                         {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'}
                       ]
                   );
               }}>
                    <Icon name="map-marker" size={20} color="#fff" />
              </TouchableHighlight>

              <TouchableHighlight style={{
                 marginLeft:30,
                 margin:10 }}
                 onPress={()=>Actions.forms({portfolio:this.props.portfolio, survey:this.props.plan.survey_id})}>
                  <Icon name="file-text-o" size={20} color="#fff" />
            </TouchableHighlight>

         </View>
       );
   },

   _renderHeader(rowData) {
     return (
         <View style={{
            paddingTop: 15,
            paddingRight: 15,
            paddingLeft: 15,
            paddingBottom: 15,
            borderBottomWidth: 0.5,
            borderBottomColor: '#566D7E',
            backgroundColor: '#5EB3E9',
            flexDirection: 'row',
            flex: 2
          }}>
            <View style={{
              flex:1
            }}>
            <Icon name="map-pin" size={30} color="#fff" />
                <Text style={{
                  paddingTop: 15,
                  paddingRight: 15,
                  paddingBottom: 15,
                  paddingLeft: 15,
                  color: '#FFF',
                  fontSize: 15,
                  fontWeight: 'bold'
                }}>{rowData.name}</Text>
            </View>

            <View style={{
                alignItems: 'flex-end',
                flex:1
                }}>
                <Text style={{
                  paddingRight: 5,
                  paddingLeft: 15,
                  color: '#FFF',
                  fontSize: 40,
                  fontWeight: 'bold'
              }}>{rowData.target}</Text>
              <Text style={{
                paddingRight: 15,
                paddingBottom: 10,
                paddingLeft: 10,
                color: '#FFF',
                fontSize: 12,
                fontWeight: 'bold'
            }}>responses</Text>
            </View>

       </View>
     );
   },

  render() {
    var _scrollView: ScrollView;
    return (
      <View style={styles.containerMap}>
        <MapView
                style={styles.map}
                initialRegion={{
                    latitude: 14.0701629,
                    longitude:100.6050414,
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.0121,
                }}
                >

                {this.props.plan['areas'].map(marker => (
                   <MapView.Marker
                     coordinate={{latitude: marker.location.lat, longitude: marker.location.lng}}
                     title={marker.name}
                     description={'Target:'+marker.target}
                   />
                 ))}

                 {this.props.plan['areas'].map(marker => (
                    <MapView.Circle
                      center={{latitude: marker.location.lat, longitude: marker.location.lng}}
                      radius={marker.radius}
                      fillColor="rgba(200, 0, 0, 0.5)"
                      strokeColor="rgba(0,0,0,0.5)"
                    />
                  ))}
          </MapView>
        <View style={styles.eventList}>
            <ScrollView
              ref={(scrollView) => { _scrollView = scrollView; }}
              automaticallyAdjustContentInsets={false}>
                  <Accordion
                      sections={this.props.plan['areas']}
                      renderHeader={this._renderHeader}
                      renderContent={this._renderContent}
                  />
              </ScrollView>
        </View>

        </View>
    );
  }
});
module.exports = Plans;
