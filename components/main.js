var React = require('react');
var ReactNative = require('react-native');
var Button = require('react-native-button');
var Icon = require('react-native-vector-icons/FontAwesome');
var Actions = require('react-native-router-flux').Actions;
const {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Component,
} = ReactNative;

const styles = StyleSheet.create({
  menuButton: {
    position: 'absolute',
    top: 20,
    padding: 10
  },
  caption: {
    fontSize: 20,
    fontWeight: 'bold',
    alignItems: 'center'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFF',
    padding: 20
  },
  mark: {
      width: 180,
      height: 180
  },
  buttonContainer: {
     padding:10,
     height:45,
     width: 200,
     borderRadius:4,
     backgroundColor: '#38ACEC',
     margin: 10
 },
 button: {
     fontFamily: 'Cochin',
     color: '#FFF'
 },
 groupButton: {
     padding: 30
 }
});

var Main = React.createClass ({
    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.mark} source={require('./logo.png')} />
                <View style={styles.groupButton}>
                    <Button
                        containerStyle={styles.buttonContainer}
                        style={styles.button}
                        onPress={()=>Actions.surveys({portfolio : this.props.portfolio})}>
                        Surveys
                    </Button>
                    <Button
                        containerStyle={styles.buttonContainer}
                        style={styles.button}
                        onPress={Actions.responses({portfolio : this.props.portfolio})}>
                        Responses
                    </Button>
                </View>
            </View>
        );
    }
});

module.exports = Main;
