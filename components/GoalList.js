var React = require('react');
var ReactNative = require('react-native');
var Button = require('react-native-button');
var Icon = require('react-native-vector-icons/FontAwesome');
var Actions = require('react-native-router-flux').Actions;
var _ = require('lodash');
const {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  Component,
  ListView,
  AsyncStorage,
} = ReactNative;

const styles = StyleSheet.create({
  buttonContainer: {
     padding: 10,
     height: 45,
     width: 250,
     borderRadius: 4,
     backgroundColor: '#38ACEC',
     margin: 5
 },
 button: {
     fontFamily: 'Cochin',
     color: '#FFF'
 },
 head: {
     marginLeft: 50,
     marginRight: 30,
     marginTop: 20,
     alignItems: 'flex-end'
 },
 icon: {
     marginRight:5
 },
 headContainerLeft: {
     alignSelf: 'stretch',
     marginTop: 40,
     marginLeft: 10
 },
 item: {
     fontFamily: 'Cochin',
     fontSize: 22,
     fontWeight: 'bold',
     padding: 2,
     color: '#222222'
 },
 Container: {
     alignItems: 'center',
     marginTop: 20
 }
});

var STORAGE_KEY = '@mdcaMobile:goals';

var GoalList = React.createClass ({
    getInitialState () {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.loadGoals();
        return {
            dataSource: ds.cloneWithRows([])
        };
    },

    async saveGoals(goals) {
        console.log('save-goals-from-storage');
        try {
            await AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(goals));
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    },

    async loadGoals() {
        console.log('load-goals-from-storage');
        try {
          var goals = await AsyncStorage.getItem(STORAGE_KEY);
          if (goals !== null) {
            this.setState({goals:JSON.parse(goals)})
            this.setState({
                dataSource: this.state.dataSource.cloneWithRows(this.state.goals)
            });
          }
        } catch (error) {
          console.log('AsyncStorage error: ' + error.message);
        }
    },

    getGoals() {
      fetch(this.props.implementation['@id']+'/goals', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
      })
      .then((response) => {
          return response.json();
      })
      .then((responses) => {
          var goals = responses;
          this.saveGoals(goals);
          this.loadGoals();
          return goals
      })
       .catch((error) => {
           console.warn(error);
       });
   },

   _renderRow (rowData) {
       return <Button
                   containerStyle={styles.buttonContainer}
                   style={styles.button}
                   onPress={()=>Actions.plans({portfolio:this.props.portfolio, goal:rowData})}>
                   {rowData['name']}
               </Button>
   },

    render() {
        return (
            <View style={styles.head}>
                <View style={styles.headContainerRight}>
                    <TouchableHighlight style={styles.icon} onPress={this.getGoals}>
                    <Icon name="cloud-download" size={30} color="#38ACEC"/>
                    </TouchableHighlight>
                </View>
                <View style={styles.headContainerLeft}>
                    <Text style={styles.item}>Goals</Text>
                </View>
                <View style={styles.Container}>
                    <ListView
                      dataSource={this.state.dataSource}
                      renderRow={this._renderRow}
                      enableEmptySections
                      />
                </View>
            </View>
        );
    }
});

module.exports = GoalList;
