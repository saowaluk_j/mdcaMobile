var React = require('react');
var ReactNative = require('react-native');
var Dimensions = require('Dimensions');
var windowSize = Dimensions.get('window');
var Actions = require('react-native-router-flux').Actions;
var Icon = require('react-native-vector-icons/FontAwesome');

var {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Component,
  Alert,
} = ReactNative;

var loginPage = React.createClass({
  getInitialState () {
      return {
          email:'',
          password:''
      }
  },
  _loginPress (event){
    console.log('Pressed!');
    fetch('http://iff.proteus-agility.com:8009/login/', {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json'
      },
      body: JSON.stringify({
         "email": this.state.email,
         "password": this.state.password
        }),
    })
    .then ((response) => {
        console.log(response.status);
        console.log('ok '+response.ok);
        if (response.status == 200) {
            Alert.alert(
                'Login.',
                'You were successfully loged in.',
                [
                  {text: 'Ok', onPress: (event) => this.goToPortfolioPage()}
                ]
            );

        }
        else {
            Alert.alert(
                'Can not signing in',
                'wrong password or wrong email.',
            );
        }

    })
     .catch((error) => {
       console.warn(error);
     });
 },
 goToPortfolioPage() {
     Actions.portfolios();
 },
 _onEmailChange (email) {
    console.log(email);
    this.setState({email});
 },
  render () {
      return (
          <View style={styles.container}>
              <View style={styles.header}>
                  <Image style={styles.mark} source={require('./logo.png')} />
              </View>
              <View style={styles.inputs}>
                  <View style={styles.inputContainer}>
                      <Icon name="user" size={24} color="#38ACEC" />
                      <TextInput
                          style={[styles.input, styles.butterflyBlueFont]}
                          placeholder="Email"
                          placeholderTextColor="#38ACEC"
                          onChangeText={this._onEmailChange}
                          value={this.state.email}
                      />
                  </View>
                  <View style={styles.inputContainer}>
                      <Icon name="unlock-alt" size={24} color="#38ACEC" />
                      <TextInput
                          password={true}
                          style={[styles.input, styles.butterflyBlueFont]}
                          placeholder="Password"
                          placeholderTextColor="#38ACEC"
                          onChangeText={(password) => this.setState({password})}
                          value={this.state.password}
                      />
                  </View>
              </View>
              <View style={styles.login}>
                  <Text style={styles.whiteFont} onPress={this._loginPress}>Login</Text>
              </View>
              <View style={styles.foot}>
              </View>
          </View>
      );
    }
  });

  var styles = StyleSheet.create({
      container: {
        flexDirection: 'column',
        flex: 1,
        backgroundColor: '#FFFFFF',
        marginTop: 40
      },
      bg: {
          position: 'absolute',
          left: 0,
          top: 0,
          backgroundColor: '#FFF',
          width: windowSize.width,
          height: windowSize.height
      },
      header: {
          justifyContent: 'center',
          alignItems: 'center',
          flex: .5,
          backgroundColor: '#FFF',
      },
      mark: {
          width: 180,
          height: 180
      },
      login: {
          padding: 20,
          alignItems: 'center',
          backgroundColor: '#38ACEC'
      },
      inputs: {
          marginTop: 10,
          marginBottom: 10,
          flex: .25
      },
      inputPassword: {
          marginLeft: 15,
          width: 20,
          height: 30
      },
      inputUsername: {
        marginLeft: 15,
        width: 20,
        height: 30
      },
      inputContainer: {
          padding: 10,
          borderWidth: 1,
          borderBottomColor: '#CCC',
          borderColor: '#D8D8D8'
      },
      input: {
          position: 'absolute',
          left: 50,
          top: 12,
          right: 10,
          height: 30,
          fontSize: 12,
          fontFamily: 'Cochin',
      },
      foot: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: .15
      },
      greyFont: {
        color: '#D8D8D8'
      },
      whiteFont: {
        color: '#FFFF',
        fontSize: 15,
        fontWeight: 'bold'
      },
      butterflyBlueFont: {
        color: '#38ACEC',
        fontSize: 13,
        fontWeight: 'bold'
      }
  });

module.exports = loginPage;
