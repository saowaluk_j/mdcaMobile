var React = require('react');
var ReactNative = require('react-native');
var Button = require('react-native-button');
var Icon = require('react-native-vector-icons/FontAwesome');
var Actions = require('react-native-router-flux').Actions;
var _ = require('lodash');
const {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  Component,
  ListView,
  AsyncStorage,
  TextInput,
  ToastAndroid,
} = ReactNative;

const styles = StyleSheet.create({
  buttonContainer: {
     padding: 10,
     height: 45,
     width: 250,
     borderRadius: 4,
     backgroundColor: '#38ACEC',
     margin: 5
 },
 button: {
     fontFamily: 'Cochin',
     color: '#FFF'
 },
 head: {
     marginLeft: 50,
     marginRight: 30,
     marginTop: 20,
     alignItems: 'flex-end'
 },
 headContainerRight: {
     flexDirection: 'row',
     padding: 3
 },
 icon: {
     marginRight:5
 },
 headContainerLeft: {
     alignSelf: 'stretch',
     marginTop: 5,
     marginLeft: 10
 },
 item: {
     fontFamily: 'Cochin',
     fontSize: 22,
     fontWeight: 'bold',
     padding: 2,
     color: '#222222'
 },
 Container: {
     alignItems: 'center',
     marginTop: 20
 }
});

var STORAGE_KEY = '@mdcaMobile:surveys';
var STORAGE_KEY_FORM = '@mdcaMobile:forms';
var Surveys = React.createClass ({
    getInitialState () {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.loadForms();
        this.loadSurveys();
        return {
            dataSource: ds.cloneWithRows([])
        };
    },

    async saveSurveys(surveys) {
        console.log('save-Surveys-from-storage');
        try {
            await AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(surveys));
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    },

    async loadSurveys() {
        console.log('load-Surveys-from-storage');
        try {
          var surveys = await AsyncStorage.getItem(STORAGE_KEY);
          surveys = JSON.parse(surveys);
          this.setState({surveys: surveys});
          if (surveys[this.props.portfolio.slug] !== null) {
            this.setState({survey:surveys[this.props.portfolio.slug]})
            this.setState({
                dataSource: this.state.dataSource.cloneWithRows(this.state.survey)
            });
          }
        } catch (error) {
          console.log('AsyncStorage error: ' + error.message);
        }
    },

    async saveForms(forms) {
        console.log('save-forms-from-storage');
        try {
            await AsyncStorage.setItem(STORAGE_KEY_FORM, JSON.stringify(forms));
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    },

    async loadForms() {
        console.log('load-forms-from-storage');
        try {
          var forms = await AsyncStorage.getItem(STORAGE_KEY_FORM);
          if(forms !== null) {
              forms = JSON.parse(forms);
              this.setState({forms: forms});
          }
        } catch (error) {
          console.log('AsyncStorage error: ' + error.message);
        }
    },

    async getForms(surveys) {
        var _this = this;
        var urls = [];
        for (var i = 0; i < surveys.length; i++) {
            urls.push('http://'+ this.props.portfolio.name+'.iff.proteus-agility.com:8009/survey/'+surveys[i]['@url']);
        }

        var promises = urls.map(function(url) {
            return fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                },
            })
        });

        Promise.all(promises).then(function(responses) {
            var formList = responses.map(function(response){
                return response.json();
            })
            return Promise.all(formList);
        })
        .then(function(formList){
            console.log(formList);
            var newFormList = _this.state.forms;
            for (var i = 0; i < formList.length; i++) {
                var newForms={};
                if(newFormList == null || Object.getOwnPropertyNames(newFormList).length === 0) {
                    newForms[_this.props.portfolio.slug] = {};
                    newForms[_this.props.portfolio.slug][formList[i]['@id']] = formList[i];
                }
                else if(newFormList[_this.props.portfolio.slug]) {
                    var newForm = {};
                    newForm[_this.props.portfolio.slug] = {};
                    newForm[_this.props.portfolio.name][formList[i]['@id']] = formList[i];
                    newForm = Object.assign(newFormList[_this.props.portfolio.slug], newForm[_this.props.portfolio.slug]);
                    newForms[_this.props.portfolio.slug] = newForm;
                }
                else {
                    var newForm = {};
                    newForm[_this.props.portfolio.slug] = {};
                    newForm[_this.props.portfolio.slug][formList[i]['@id']] = formList[i];
                    newForms = newFormList;
                    newForms = Object.assign(_this.state.forms, newForm);
                }
                newFormList = Object.assign(newFormList,newForms);
            }
            return newFormList;
        })
        .then(function(newFormList){
            return _this.saveForms(newFormList);
        })
        .then(function(){
            return _this.loadForms();
        })
        .then(function(){
            ToastAndroid.show('Survey list was loaded.', ToastAndroid.SHORT);
        })
        .catch((error) => {
            console.warn(error);
        });
   },

    getSurveys() {
        console.log('get-Surveys-from-web');
        fetch('http://'+ this.props.portfolio.name+'.iff.proteus-agility.com:8009/survey/', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        })
        .then((response) => {
            return response.json();
        })
        .then((responses) => {
             var surveys = [];
             for (response in responses) {
                 if (responses[response] !== null && typeof responses[response] === 'object') {
                     surveys.push(responses[response]);
                 }
             }
             this.getForms(surveys);
             var newSurveys = {};
             newSurveys[this.props.portfolio.slug] = surveys;
             if(this.state.surveys == null) {
                 this.saveSurveys(newSurveys);
                 this.loadSurveys();
             }
             else {
                 newSurveys = Object.assign(this.state.surveys, newSurveys);
                 this.saveSurveys(newSurveys);
                 this.loadSurveys();
             }
             return surveys
        })
        .catch((error) => {
            console.warn(error);
        });
   },

   getDataSource(){
       if (!this.state.filter) {
           return this.state.dataSource
       }
       var filtered = this.state.survey.filter((entry) => entry['@id'].indexOf(this.state.filter) >= 0);
       return this.state.dataSource.cloneWithRows(filtered);
   },

   onFilterChange(event){
       this.setState({
           filter: event.nativeEvent.text
       });
   },

   _renderRow (rowData) {
       return <Button
                   containerStyle={styles.buttonContainer}
                   style={styles.button}
                   onPress={()=>Actions.forms({portfolio:this.props.portfolio, survey:rowData['@id']})}>
                   {rowData['@id']}
               </Button>
   },

    render() {
        return (
            <View style={styles.head}>
                <View style={styles.headContainerRight}>
                    <TouchableHighlight style={styles.icon} onPress={this.getSurveys}>
                    <Icon name="cloud-download" size={30} color="#38ACEC"/>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.icon}>
                    <Icon name="cloud-upload" size={30} color="#38ACEC"/>
                    </TouchableHighlight>
                </View>
                <View style={styles.headContainerLeft}>
                    <Text style={styles.item}>Survey List</Text>
                </View>
                <TextInput style={styles.input}
                           placeholder={'filter'}
                           value={this.state.filter}
                           onChange={this.onFilterChange}/>
                <View style={styles.Container}>
                    <ListView
                      dataSource={this.getDataSource()}
                      renderRow={this._renderRow}
                      enableEmptySections
                      />
                </View>
            </View>
        );
    }
});

module.exports = Surveys;
