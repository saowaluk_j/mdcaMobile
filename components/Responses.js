var React = require('react');
var ReactNative = require('react-native');
var Button = require('react-native-button');
var Icon = require('react-native-vector-icons/FontAwesome');
var Actions = require('react-native-router-flux').Actions;
const {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Component,
  TouchableHighlight,
  ListView,
  AsyncStorage,
  Alert,
} = ReactNative;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonContainer: {
     padding:10,
     height:45,
     borderRadius:4,
     backgroundColor: '#38ACEC',
     margin: 20
 },
 button: {
     fontFamily: 'Cochin',
     color: '#FFF'
 },
 head: {
     margin: 50,
     marginTop: 40,
     alignItems: 'flex-end'
 },
 headContainerRight: {
     flexDirection: 'row',
     padding: 3
 },
 headContainerLeft: {
     alignSelf: 'stretch',
     marginTop: 10
 },
 icon: {
     right: 10
 },
 item: {
     fontFamily: 'Cochin',
     fontSize: 22,
     fontWeight: 'bold',
     padding: 2,
     color: '#222222'
 },
 responseText: {
     fontFamily: 'Cochin',
     fontSize: 17,
     padding: 2,
     color: '#FFFFFF'
 },
 responseBox: {
     flex: 1,
     height: 100,
     width: 270,
     justifyContent: 'center',
     backgroundColor: '#38ACEC',
     borderRadius: 8,
     margin: 10,
     marginLeft: 60,
     padding: 30,

 }
});

var STORAGE_KEY_RESPONSE = '@mdcaMobile:responses';

var Responses = React.createClass ({
    getInitialState () {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.loadResponses();
        return {
            dataSource: ds.cloneWithRows([])
        }
    },

    async saveResponse(newResponseList) {
        try {
            await AsyncStorage.setItem(STORAGE_KEY_RESPONSE, JSON.stringify(newResponseList));
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    },

    async loadResponses() {
        console.log('load-responses-from-storage');
        try {
          var responses = await AsyncStorage.getItem(STORAGE_KEY_RESPONSE);
          if (responses !== null) {
              this.setState({responses:JSON.parse(responses)});
              this.setState({
                  dataSource: this.state.dataSource.cloneWithRows(this.state.responses)
              });
              console.log(this.state.responses);
          }
          else {
              this.setState({responses:{}});
          }
        } catch (error) {
          console.log('AsyncStorage error: ' + error.message);
        }
    },

    putResponse (response) {
        var url = 'http://'+ this.props.portfolio.name+'.iff.proteus-agility.com:8009/response/'
                    +response.meta['response_id']+'/';
        return fetch (url, {
           method: 'PUT',
           headers: {
               'Content-Type': 'application/json'
           },
           body: JSON.stringify(response)
        })
        .then((response) => {
            debugger;
            console.log(response);
           return response;
        })
        .catch((error) => {
           console.warn(error);
        });
    },

    upLoadResponses() {
        console.log(this.state.responses);
        var responses = this.state.responses;
        var promises = [];
        var keys = [];
        for (var response in responses) {
            console.log(response);
            keys.push(response);
            promises.push(this.putResponse(responses[response]));
        }
        Promise.all(promises).then((responses) => {
            console.log(responses);
            return this.saveResponse({});
        })
        .then((response) => {
            Alert.alert(
                'Uploaded.',
                'Responses have been uploaded.',
                [
                  {text: 'Ok', onPress: () => this.loadResponses()}
                ]
            );
        })
        .catch((error) => {
           console.warn(error);
        });
    },

    _renderRow (rowData) {
        console.log('row',rowData);
        return <View style={styles.responseBox}>
                    <Text style={styles.responseText}>Survey: {rowData.meta['survey_slug']}</Text>
                    <Text style={styles.responseText}>ID: {rowData.meta['response_id']}</Text>
                    <Text style={styles.responseText}>Enumerator: {rowData.meta['enumerator']}</Text>
        </View>
    },

    render() {
        return (
            <View style={styles.head}>
                <View style={styles.headContainerRight}>
                    <TouchableHighlight style={styles.icon} onPress={this.upLoadResponses}>
                        <Icon name="cloud-upload" size={30} color="#38ACEC"/>
                    </TouchableHighlight>
                </View>
                <View style={styles.headContainerLeft}>
                    <Text style={styles.item}>Responses</Text>
                </View>
                <ListView
                  dataSource={this.state.dataSource}
                  renderRow={this._renderRow}
                  enableEmptySections
                  />
            </View>
        );
    }
});

module.exports = Responses;
