var React = require('react');
var ReactNative = require('react-native');
var Button = require('react-native-button');
var Icon = require('react-native-vector-icons/FontAwesome');
var Actions = require('react-native-router-flux').Actions;
var _ = require('lodash');
const {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  Component,
  ListView,
  AsyncStorage,
  TextInput,
  ToastAndroid,
} = ReactNative;

const styles = StyleSheet.create({
  buttonContainer: {
     padding: 10,
     height: 45,
     width: 250,
     borderRadius: 4,
     backgroundColor: '#38ACEC',
     margin: 5
 },
 button: {
     fontFamily: 'Cochin',
     color: '#FFF'
 },
 head: {
     marginLeft: 50,
     marginRight: 30,
     marginTop: 20,
     alignItems: 'flex-end'
 },
 headContainerRight: {
     flexDirection: 'row',
     padding: 3
 },
 icon: {
     marginRight:5
 },
 headContainerLeft: {
     alignSelf: 'stretch',
     marginTop: 5,
     marginLeft: 10
 },
 item: {
     fontFamily: 'Cochin',
     fontSize: 22,
     fontWeight: 'bold',
     padding: 2,
     color: '#222222'
 },
 Container: {
     alignItems: 'center',
     marginTop: 20
 }
});

var STORAGE_KEY = '@mdcaMobile:missions';
var STORAGE_KEY_PLAN = '@mdcaMobile:plans';

var PlanList = React.createClass ({
    getInitialState () {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.loadMissions();
        this.loadPlans();
        return {
            dataSource: ds.cloneWithRows([])
        }
    },

    async saveMissions(missions) {
        console.log('save-missions-from-storage');
        try {
            await AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(missions));
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    },

    async loadMissions() {
        console.log('load-missions-from-storage');
        try {
          var missions = await AsyncStorage.getItem(STORAGE_KEY);
          missions = JSON.parse(missions);
          this.setState({missions: missions});
          if (missions[this.props.portfolio.slug] !== null) {
            this.setState({mission:missions[this.props.portfolio.slug]})
          }
        } catch (error) {
          console.log('AsyncStorage error: ' + error.message);
        }
    },

    async savePlans(plans) {
        console.log('save-plans-from-storage');
        try {
            await AsyncStorage.setItem(STORAGE_KEY_PLAN, JSON.stringify(plans));
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    },

    async loadPlans() {
        console.log('load-plans-from-storage');
        try {
          var plans = await AsyncStorage.getItem(STORAGE_KEY_PLAN);
          plans = JSON.parse(plans);
          if (plans[this.props.portfolio.slug] !== null) {
            this.setState({plans:plans})
            this.setState({
                dataSource: this.state.dataSource.cloneWithRows(this.state.plans[this.props.portfolio.slug])
            });
          }
        } catch (error) {
          console.log('AsyncStorage error: ' + error.message);
        }
    },

    getMissions() {
      fetch('http://'+ this.props.portfolio.name+'.iff.proteus-agility.com:8009/missions', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
      })
      .then((response) => {
          return response.json();
      })
      .then((responses) => {
          var missions = responses;
          var newMissions = {};
          newMissions[this.props.portfolio.slug] = missions;
          if(this.state.missions == null) {
              this.saveMissions(newMissions);
              this.loadMissions();
          }
          else {
              newMissions = Object.assign(this.state.missions, newMissions);
              this.saveMissions(newMissions);
              this.loadMissions();
          }
          console.log('-newmission-', newMissions);
          return newMissions;
      })
      .then((newMissions) => {
          return this.getImplementations(newMissions[this.props.portfolio.slug]);
      })
       .catch((error) => {
         console.warn(error);
       });
   },

   getImplementations(missions) {
       var _this = this;
       var urls = [];
       for (var i = 0; i < missions.length; i++) {
           urls.push(missions[i].links.implementations.read.url);
       }

       var promises = urls.map(function(url) {
           return fetch(url, {
               method: 'GET',
               headers: {
                   'Content-Type': 'application/json'
               },
           })
       });

       Promise.all(promises).then(function(responses) {
           var implementations = responses.map(function(response){
               return response.json();
           })
           return Promise.all(implementations);
       })
       .then(function(implementations){
           console.log(implementations);
           var impList = [];
           for (var i = 0; i < implementations.length; i++) {
               impList = impList.concat(implementations[i]);
           }
           console.log('merged imp', impList);
           return impList;
       })
       .then((impList) => {
           return this.getGoalList(impList);
       })
       .catch((error) => {
           console.warn(error);
       });
  },

  getGoalList(impList) {
      var _this = this;
      var urls = [];
      for (var i = 0; i < impList.length; i++) {
          urls.push(impList[i]['@id']);
      }

      var promises = urls.map(function(url) {
          return fetch(url, {
              method: 'GET',
              headers: {
                  'Content-Type': 'application/json'
              },
          })
      });

      Promise.all(promises).then(function(responses) {
          var goals = responses.map(function(response){
              return response.json();
          })
          return Promise.all(goals);
      })
      .then(function(goals){
          var promises=[];
          var goalList = [];
          for (var i = 0; i < goals.length; i++) {
              if(goals[i].goals.length !== 0){
                  promises.push(_this.getPlanList(goals[i]));
              }
          }
          return Promise.all(promises);
      })
      .then(function(planList) {
          var plans = [];
          for (var i = 0; i < planList.length; i++) {
              if(planList[i].length !== 0){
                  plans = plans.concat(planList[i]);
              }
          }
          var newPlans = {};
          newPlans[_this.props.portfolio.slug] = plans;
          if(_this.state.plans == null) {
              _this.savePlans(newPlans);
          }
          else {
              newPlans = Object.assign(_this.state.plans, newPlans);
              _this.savePlans(newPlans);
          }
          return newPlans;
      })
      .then(function() {
          _this.loadPlans();
      })
      .then(function() {
          ToastAndroid.show('Plan list was loaded.', ToastAndroid.SHORT);
      })
      .catch((error) => {
          console.warn(error);
      });
  },

  getPlanList(goalList) {
      var survey = goalList.survey;
      var _this = this;
      var urls = [];
      for (var i = 0; i < goalList.goals.length; i++) {
          urls.push(goalList.goals[i]['@id']+'/plan');
      }

      var promises = urls.map(function(url) {
          return fetch(url, {
              method: 'GET',
              headers: {
                  'Content-Type': 'application/json'
              },
          })
      });
      promises.push(survey);

      return Promise.all(promises).then(function(responses) {
          var plans = responses.map(function(response){
              if(typeof response === 'string') {
                  return response;
              }
              return response.json();
          })
          return Promise.all(plans);
      })
      .then(function(plans){
          var planList = [];
          for (var i = 0; i < plans.length-1; i++) {
              plans[i].survey_id = plans[plans.length-1];
              planList.push(plans[i]);
          }
          return planList;
      })
      .catch((error) => {
          console.warn(error);
      });
  },

  getDataSource(){
      if (!this.state.filter) {
          return this.state.dataSource
      }
      var filtered = this.state.plans[this.props.portfolio.slug].filter((entry) => entry.name.indexOf(this.state.filter) >= 0);
      return this.state.dataSource.cloneWithRows(filtered);
  },

  onFilterChange(event){
      this.setState({
          filter: event.nativeEvent.text
      });
  },

   _renderRow (rowData) {
       return <Button
                   containerStyle={styles.buttonContainer}
                   style={styles.button}
                   onPress={()=>Actions.plans({portfolio:this.props.portfolio,plan:rowData})}>
                   {rowData['name']}
               </Button>
   },

    render() {
        return (
            <View style={styles.head}>
                <View style={styles.headContainerRight}>
                    <TouchableHighlight style={styles.icon} onPress={this.getMissions}>
                        <Icon name="cloud-download" size={30} color="#38ACEC"/>
                    </TouchableHighlight>
                </View>
                <View style={styles.headContainerLeft}>
                    <Text style={styles.item}>Plan List</Text>
                </View>
                <TextInput style={styles.input}
                           placeholder={'filter'}
                           value={this.state.filter}
                           onChange={this.onFilterChange}/>
                <View style={styles.Container}>
                    <ListView
                      dataSource={this.getDataSource()}
                      renderRow={this._renderRow}
                      enableEmptySections
                      />
                </View>
            </View>
        );
    }
});

module.exports = PlanList;
