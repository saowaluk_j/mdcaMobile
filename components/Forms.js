var React = require('react');
var ReactNative = require('react-native');
var Button = require('react-native-button');
var Icon = require('react-native-vector-icons/FontAwesome');
var Actions = require('react-native-router-flux').Actions;
var moment = require('moment');
var _ = require('lodash');
const {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  RecyclerViewBackedScrollView,
  Component,
  ScrollView,
  ListView,
  Picker,
  DatePickerAndroid,
  TouchableWithoutFeedback,
  AsyncStorage,
  Alert,
} = ReactNative;

const styles = StyleSheet.create({
  buttonContainer: {
     padding: 7,
     height: 35,
     width: 90,
     borderRadius: 4,
     backgroundColor: '#38ACEC',
     margin: 10
 },
 scroll: {
    flex:1,
    height:587,
 },
 button: {
     fontFamily: 'Cochin',
     fontSize: 15,
     color: '#FFF'
 },
 button2: {
     fontFamily: 'Cochin',
     fontSize: 12,
     color: '#FFF'
 },
 head: {
     margin: 10,
     marginTop: 60,
     flexDirection: 'row'
 },
 container: {
     marginLeft: 40,
     marginRight: 30,
     marginTop: 20
 },
 viewSurveyName: {
     marginLeft: 30,
     marginRight: 30,
     marginTop: 5
 },
 item: {
     fontFamily: 'Cochin',
     fontSize: 15,
     fontWeight: 'bold'
 },
 inputBox: {
     marginTop:10,
     height: 30,
     borderColor: 'grey',
     borderWidth: 1,
     backgroundColor: 'white'
 },
 submitButton: {
    backgroundColor: '#38ACEC',
    margin: 20,
    marginRight: 10,
 },
 locationButton: {
     paddingTop: 4,
     height: 25,
     width: 110,
     marginTop: 10,
     marginRight: 20,
     borderRadius: 4,
     backgroundColor: '#38ACEC'
 },
 viewSubmitButton: {
     marginTop: 30,
     alignItems: 'flex-end'
 },
 viewAll: {
     flex:1
 }
});

var STORAGE_KEY_FORM = '@mdcaMobile:forms';
var STORAGE_KEY_RESPONSE = '@mdcaMobile:responses';

var Forms = React.createClass ({
    getInitialState () {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this._getCurrentPosition();
        this.loadForms();
        this.loadResponses();
        return {
            dataSource: ds.cloneWithRows([]),
            section:'',
            surveyName: this.props.survey,
            response:{response_id: this.get_timestamp()+'-'+this.make_id(),
                        '@context': 'surveyresponse',
                        status:'',
                        answers:{
                            meta: {
                                valid: true,
                                visited: true
                            }
                        },
                        links: {
                            self:{
                                delete:{
                                    method:'DELETE',
                                    url:''
                                },
                                update:{
                                    method: 'PUT',
                                    url:''
                                }
                            }
                        },
                       meta: {
                           all_valid: false,
                           all_visited: false,
                           response_id: this.get_timestamp()+'-'+this.make_id(),
                           enumerator: 'iffer',
                           loginId: 'testuser@proteus-tech.com',
                           portfolio: this.props.portfolio.name,
                           response_status: 'In Progress',
                           server: 'http://'+ this.props.portfolio.name+'.iff.proteus-agility.com:8009/',
                           start_time: moment().utc().format(),
                           survey_slug:''
                       }
            },

        };
    },

    async loadForms() {
        console.log('load-forms-from-storage');
        try {
          var forms = await AsyncStorage.getItem(STORAGE_KEY_FORM);
          if(forms !== null) {
              forms = JSON.parse(forms);
              this.setState({forms: forms});
              this.setState({questions: forms[this.props.portfolio.name][this.props.survey].questions});
              this.setResponse(forms[this.props.portfolio.name][this.props.survey]);
              this.setState({
                  dataSource: this.state.dataSource.cloneWithRows(this.state.questions)
              });
          }
        } catch (error) {
          console.log('AsyncStorage error: ' + error.message);
        }
    },

    async saveResponse(newResponseList) {
        try {
            await AsyncStorage.setItem(STORAGE_KEY_RESPONSE, JSON.stringify(newResponseList));
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    },

    async loadResponses() {
        console.log('load-responses-from-storage');
        try {
          var responses = await AsyncStorage.getItem(STORAGE_KEY_RESPONSE);
          if (responses !== null) {
              this.setState({responses:JSON.parse(responses)});
          }
          else {
              this.setState({responses:{}});
          }
        } catch (error) {
          console.log('AsyncStorage error: ' + error.message);
        }
    },

    setResponse (responses) {
        var section = responses.survey.sections[0];
        var split = section.split('/');
        var url = 'http://'+ this.props.portfolio.name+'.iff.proteus-agility.com:8009/mission/'
                                        +responses.mission+'/implementation/'+responses.implementation
                                        +'/survey/response/'+this.state.response.response_id;
        this.state.section = split[1];
        this.state.response.answers[this.state.section] = {};
        this.state.response.meta.survey_slug = responses.survey.slug;
        this.state.response.links.self.delete.url = url;
        this.state.response.links.self.update.url =  url;
        console.log('response', this.state.response);
   },

   storeResponse() {
       this.state.response.meta.response_status = 'Completed';
       var newResponse = {};
       newResponse[this.state.response.response_id] = {
                                                           answers: this.state.response.answers,
                                                           meta: this.state.response.meta
                                                       };
       var newResponseList = Object.assign(this.state.responses, newResponse);
       return this.saveResponse(newResponseList)
                .then(()=> {
                   return this.loadResponses();
               })
               .then(() => {
                   Alert.alert(
                       'Saved response.',
                       null,
                       [
                         {text: 'Ok', onPress: () => {
                             Actions.pop();
                             Actions.refresh();
                            }
                         }
                       ]
                   );

               });
   },

   _getCurrentPosition() {
       navigator.geolocation.getCurrentPosition(
           (position) => {
             this.state.response.meta.location = position.coords;
             console.log(position);
           },
           (error) => alert(error.message)
       );
   },

   _setCurrentPosition(slug) {
       navigator.geolocation.getCurrentPosition(
           (position) => {
             this.setAnswer(slug, position.coords);
             console.log(position);
           },
           (error) => alert(error.message)
       );
   },

   async showPicker(slug, options) {
        try {
          const {action, year, month, day} = await DatePickerAndroid.open(options);
          if (action === DatePickerAndroid.dismissedAction) {
              this.setAnswer(slug, 'dismissed');
          } else {
            var date = new Date(year, month, day);
            this.setAnswer(slug, date.toLocaleDateString());
          }
        } catch ({code, message}) {
          console.warn(`can not open calendar`, message);
        }
  },

   _renderRow (rowData) {
       var input;
       if(rowData!==null && rowData!=='') {
           this.initialAnswer(rowData);
           if(rowData.context == "text") {
               input = <TextInput
                   onChangeText={(answer) => this.setAnswer(rowData.slug, answer)}
                   style={styles.inputBox}
                   />
           }
           else if(rowData.context == "numeric") {
               input = <TextInput
                   onChangeText={(answer) => this.setAnswer(rowData.slug, answer)}
                   style={styles.inputBox}
                   />
           }
           else if(rowData.context == "gps/here") {
               input = <Button
                        containerStyle={styles.locationButton}
                        style={styles.button2} onPress={this._setCurrentPosition(rowData.slug)}>
                            Detect location
                        </Button>
           }
           else if(rowData.context == "date") {
               input = <TouchableWithoutFeedback
                            onPress={this.showPicker.bind(this, rowData.slug, this.state.response.answers[this.state.section][rowData.slug].value)}>
                            <View><Text style={styles.text}>pick a date</Text></View>
                        </TouchableWithoutFeedback>
           }
           else if(rowData.context == "one-out-of-many/drop-down") {
               console.log('choices', rowData.options);
               input = <Picker
                          selectedValue={this.state.response.answers[this.state.section][rowData.slug].value}
                          onValueChange={(answer) => this.setAnswer(rowData.slug, answer)}>
                              {Object.keys(rowData.options).map((choice) => (
                                <Picker.Item
                                  key={choice}
                                  value={choice}
                                  label={choice}
                                />
                              ))}
                        </Picker>
           }
       }
       return (
            <View style={styles.container}>
                <Text style={styles.item}>{rowData.label['en']}</Text>
                {input}
            </View>
       );
   },

   make_id() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    },

    get_timestamp() {
        new Date().getTime();
        return Date.now();
    },

    setAnswer(questionSlug, answer) {
        this.state.response.answers[this.state.section][questionSlug].completed = moment().utc().format();
        this.state.response.answers[this.state.section][questionSlug].value = answer;
    },

    initialAnswer(rowData) {
        this.state.response.answers[this.state.section][rowData.slug]={};
        this.state.response.answers[this.state.section][rowData.slug].start = moment().utc().format();
        this.state.response.answers[this.state.section][rowData.slug].visible = true;
    },

    _renderHeader() {
        return (
            <View style={styles.viewSurveyName}>
                <Text style={styles.item}>
                    Survey : {this.state.surveyName}
                </Text>
            </View>
            );
    },

  render() {
    return (
        <View style={styles.viewAll}>
            <View style={styles.head}>
                <Button
                    containerStyle={styles.buttonContainer}
                    style={styles.button}
                    onPress={Actions.surveys}>
                    Previous
                </Button>
                <Button
                    containerStyle={styles.buttonContainer}
                    style={styles.button}>
                    Summary
                </Button>
                <Button
                    containerStyle={styles.buttonContainer}
                    style={styles.button}>
                    Next
                </Button>
            </View>
                <ListView
                  dataSource={this.state.dataSource}
                  renderRow={this._renderRow}
                  renderHeader={this._renderHeader}
                  style={styles.scroll}
                  enableEmptySections
                  />
                <View style={styles.viewSubmitButton}>
                  <Button
                      containerStyle={styles.buttonContainer}
                      style={styles.button}
                      onPress={(event)=> {
                      Alert.alert(
                          'Submit?',
                          'Are you sure you want to submit this form? It will no longer be available for editing.',
                          [
                            {text: 'Ok', onPress: () => this.storeResponse()},
                            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'}
                          ]
                      );
                      console.log('submit', this.state.response)}}>
                    submit
                  </Button>
                 </View>
        </View>
    );
  }
});

module.exports = Forms;
