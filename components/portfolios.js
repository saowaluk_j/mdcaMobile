var React = require('react');
var ReactNative = require('react-native');
var Button = require('react-native-button');
var Actions = require('react-native-router-flux').Actions;
var Icon = require('react-native-vector-icons/FontAwesome');
const {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  Component,
  ListView,
  AsyncStorage,
  TextInput,
  ToastAndroid,
} = ReactNative;

const styles = StyleSheet.create({
  buttonContainer: {
     padding: 10,
     height: 45,
     width: 250,
     borderRadius: 4,
     backgroundColor: '#38ACEC',
     margin: 5
 },
 button: {
     fontFamily: 'Cochin',
     color: '#FFF'
 },
 head: {
     margin: 50,
     marginTop: 30,
     alignItems: 'flex-end'
 },
 icon: {
     right: 10
 },
 headContainerLeft: {
     alignSelf: 'stretch',
     marginTop: 25,
     marginLeft: 10
 },
 item: {
     fontFamily: 'Cochin',
     fontSize: 22,
     fontWeight: 'bold',
     padding: 2,
     color: '#222222'
 },
 Container: {
     alignItems: 'center',
     marginTop: 20
 }
});

var STORAGE_KEY = '@mdcaMobile:portfolios';

var Portfolios = React.createClass ({
    getInitialState () {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.loadPortfolios();
        return {
            dataSource: ds.cloneWithRows([])
        };
    },

    async savePortfolios(portfolios) {
        try {
            await AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(portfolios));
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    },

    async loadPortfolios() {
        console.log('load-portfolios-from-storage');
        try {
          var portfolios = await AsyncStorage.getItem(STORAGE_KEY);
          if (portfolios !== null) {
            this.setState({portfolios:JSON.parse(portfolios)})
            this.setState({
                dataSource: this.state.dataSource.cloneWithRows(this.state.portfolios)
            });
          }
        } catch (error) {
          console.log('AsyncStorage error: ' + error.message);
        }
    },

    getPortfolios () {
        fetch('http://iff.proteus-agility.com:8009/portfolios', {
          method: 'GET',
          headers: {
              'Content-Type': 'applicationjson'
          },
        })
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            this.savePortfolios(data);
            this.loadPortfolios();
            return data
        })
        .then(() => {
            ToastAndroid.show('Portfolio list was loaded.', ToastAndroid.SHORT);
        })
         .catch((error) => {
           console.warn(error);
         });
     },

    onFilterChange(event){
        this.setState({
            filter: event.nativeEvent.text
        });
    },

    getDataSource(){
        if (!this.state.filter) {
            return this.state.dataSource
        }
        var filtered = this.state.portfolios.filter((entry) => entry.name.indexOf(this.state.filter) >= 0);
        return this.state.dataSource.cloneWithRows(filtered);
    },

    _renderRow (rowData) {
        return <Button
                    containerStyle={styles.buttonContainer}
                    style={styles.button}
                    onPress={()=>Actions.main({portfolio : rowData})}>
                    {rowData.name}
                </Button>
    },

    render() {
        return (
            <View style={styles.head}>
                <View style={styles.headContainerRight}>
                    <TouchableHighlight style={styles.icon} onPress={this.getPortfolios}>
                    <Icon name="cloud-download" size={30} color="#38ACEC"/>
                    </TouchableHighlight>
                </View>
                <View style={styles.headContainerLeft}>
                    <Text style={styles.item}>Portfolios</Text>
                </View>
                <TextInput style={styles.input}
                           placeholder={'filter'}
                           value={this.state.filter}
                           onChange={this.onFilterChange}/>
                <View style={styles.Container}>
                    <ListView
                      dataSource={this.getDataSource()}
                      renderRow={this._renderRow}
                      enableEmptySections
                      />
                </View>
            </View>
        );
    }
});

module.exports = Portfolios;
