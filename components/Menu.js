var React = require('react');
var ReactNative = require('react-native');
var Icon = require('react-native-vector-icons/FontAwesome');
var Actions = require('react-native-router-flux').Actions;
const {
  Dimensions,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  Component,
  Alert,
} = ReactNative;

const window = Dimensions.get('window');

const styles = StyleSheet.create({
  menu: {
    flex: 1,
    width: window.width,
    height: window.height,
    backgroundColor: '#38ACEC',
    padding: 20
  },
  avatarContainer: {
    marginBottom: 20,
    marginTop: 20
  },
  avatar: {
    width: 48,
    height: 48,
    borderRadius: 24,
    flex: 1
  },
  name: {
    position: 'absolute',
    left: 70,
    top: 20
},
  item: {
    fontSize: 20,
    fontWeight: 'bold',
    padding: 10,
    color: '#FFF'
  }
});

module.exports = class Menu extends React.Component {
    render() {
        return (
          <ScrollView scrollsToTop={false} style={styles.menu}>
            <View style={styles.avatarContainer}>
              <Icon name="home" size={30} color="#FFF" />
            </View>

            <Text
              style={styles.item}
              onPress={Actions.portfolios}>
              Home
            </Text>

            <Text
              style={styles.item}
              onPress={Actions.portfolioPlan}>
              Plan
            </Text>

            <Text
              style={styles.item}
              onPress={(event)=> {
              Alert.alert(
                  'Logout.',
                  'Are you sure you want to log out?',
                  [
                    {text: 'Ok', onPress: () => Actions.loginPage()},
                    {text: 'Cancel', style: 'cancel'}
                  ]
              );}}>
              Logout
            </Text>
          </ScrollView>
        );
    }
};
